#!/bin/bash

# Help & Manual
man() {
    cat <<- EOF

Usage: ./$script_name [OPTIONS]

Options:
   -h, --help                          Displays this help
   -u, --user                          Specifies the username for password provisioning
   --password-length=<length>          Sets the length of the user's password
   --change-months=<months>            Sets the password expiration period in months
   --last-passwords-count=<count>      Specifies the number of last passwords to be remembered
   --uppercase-count=<count>           Sets the minimum count of uppercase letters in the password
   --lowercase-count=<count>           Sets the minimum count of lowercase letters in the password
   --number-count=<count>              Sets the minimum count of numbers in the password
   --special-count=<count>             Sets the minimum count of special characters in the password
   --password-lock=<1 or 0>            Locks or unlocks the user's account (1 for lock, 0 for unlock)
   --last-password-change-date=<count> Sets the last password change date
   --deny-sudo=<1 or 0>                Deny / allow 'sudo su -' & 'sudo -s' for specific
                                       user (1 - deny, 0 - allow)
   --iptables-user                     Create a user who can execute 'iptables' without 'sudo'
   --read-access-to-log                Grant read access to syslog/messages without using SUDO
   --send-email                        Send a test email to the user
   --disable-login                     Disable the user's login temporarily
   --enable-login                      Enable the user's login
   --blacklist=<file>                  Block logins from certain IPs (specify a file with IPs)
   --whitelist=<file>                  Unblock logins from certain IPs (specify a file with IPs)

   Examples:
   sudo ./$script_name -u myuser --iptables-user --read-access-to-log \\
   --password-length=10 --change-months=3 --last-passwords-count=5 \\
   --uppercase-count=2 --lowercase-count=2 --number-count=2 --special-count=2 \\
   --password-lock=1 --last-password-change-date=0 --deny-sudo=1 --disable-login \\
   --send-email --blacklist=ip.txt;
   sudo ./$script_name -u myuser --password-lock=0 --enable-login --deny-sudo=0 \\
   --send-email --whitelist=ip.txt;
   ./$script_name -h;
EOF
}

# Check if utility is installed
check_util() {
    command -v "$1" >/dev/null 2>&1
}

# Install utility using yum, apt-get, or dnf
install_util() {
    check_util yum && sudo yum install "$1" -y && return
    check_util dnf && sudo dnf install "$1" -y && return
    check_util apt-get && sudo apt-get install "$1" -y && return

    # Check if 'epel' repository is installed on Amazon Linux
    if check_util amazon-linux-extras; then
        sudo amazon-linux-extras install epel -y && sudo yum install "$1" -y && return
    fi

    echo "Error: no supported package manager found"
    return 1
}

# Ask user whether to install the utility
custom_install() {
    while true; do
        read -r -p "Want to install $1? [y/n] " input
        case $input in
            [yY])
                install_util "$1"
                break
                ;;
            [nN])
                break
                ;;
            *)
                echo "Answer (y) for yes or (n) for no."
                ;;
        esac
    done
}

# Aux function to check if 'mailx' utilitiy is installed
check_mailx() {
    if ! check_util mailx; then
        echo "mailx utility not found."
        custom_install "mailx"
    fi
}

# Function for password provisioning
provision_password() {

    # Set user's password length and expiration policy
    echo "$username:$password" | chpasswd

    # Password policies
    if [[ "$length" -eq 1 ]]; then
        sed -i "s/\(minlen=\)[0-9]\+/minlen=$password_length/g" /etc/pam.d/common-password
        echo "Password length for user $username is set to: $password_length"
    fi

    if [[ "$months" -eq 1 ]]; then
        chage -M "$change_months" "$username"
        echo "Expiration period for user $username is set to: $change_months month(s)"
    fi

    if [[ "$last" -eq 1 ]]; then
        sed -i "s/\(remember=\)[0-9]\+/remember=$last_passwords_count/g" /etc/pam.d/common-password
        echo "Last passwords to be remembered for user $username is set to: $last_passwords_count"
    fi

    if [[ "$upper" -eq 1 ]]; then
        sed -i "s/\(minupper=\)[0-9]\+/minupper=$uppercase_count/g" /etc/pam.d/common-password
        echo "Minimum count of uppercase letters for user $username is set to: $uppercase_count"
    fi

    if [[ "$lower" -eq 1 ]]; then
        sed -i "s/minlower=[0-9]\+/minlower=$lowercase_count/g" /etc/pam.d/common-password
        echo "Minimum count of lowercase letters for user $username is set to: $lowercase_count"
    fi

    if [[ "$number" -eq 1 ]]; then
        sed -i "s/\(minnum=\)[0-9]\+/minnum=$number_count/g" /etc/pam.d/common-password
        echo "Minimum count of numbers for user $username is set to: $number_count"
    fi

    if [[ "$special" -eq 1 ]]; then
        sed -i "s/\(minspecial=\)[0-9]\+/minspecial=$special_count/g" /etc/pam.d/common-password
        echo "Minimum count of special characters for user $username is set to: $special_count"
    fi

    if [[ "$date" -eq 1 ]]; then
        chage -d "$last_password_change_date" "$username"
        echo "Last password change date for user $username is set to: $last_password_change_date"
    fi

    # Perform lock/unlock user account
    if [[ "$lock" -eq 1 ]]; then
        if [[ "$password_lock" -eq 1 ]]; then
            passwd -l "$username"
        elif [[ "$password_lock" -eq 0 ]]; then
            passwd -u "$username"
        else
            echo "Invalid value for --password-lock option. Use 1 to lock or 0 to unlock."
        fi
    fi

    # Deny / allow 'sudo su -' & 'sudo -s' for specific user
    if [[ "$deny" -eq 1 ]]; then
        if [[ "$deny_sudo" -eq 1 ]]; then
            echo "$username ALL = ALL, !/bin/su, !/bin/bash" > /etc/sudoers.d/deny_"$username"_su
            echo "'sudo su -' & 'sudo -s' for user $username have been denied."
        elif [[ "$deny_sudo" -eq 0 ]]; then
            rm -f /etc/sudoers.d/deny_"$username"_su
            echo "'sudo su -' & 'sudo -s' for user $username have been allowed."
        else
            echo "Invalid value for --deny-sudo option. Use 1 to deny or 0 to allow."
        fi
    fi

    # Prevent accidental removal
    if [ -f /var/log/auth.log ]; then
        # Prevent accidental removal: (Debian)
        chattr +a /var/log/auth.log
    elif [ -f /var/log/secure ]; then
        # Prevent accidental removal: (RedHat)
        chattr +a /var/log/secure
    else
        echo "Warning: Could not find /var/log/auth.log or /var/log/secure file. Skipping prevention of accidental removal."
    fi
}

# Function to disable the user's login
disable_login() {
    if [ -z "$username" ]; then
        echo "Error: Username is missing."
        man
        exit 1
    elif [ -f /tmp/"$script_name"."$username".shell ] ||
    [ -f /tmp/"$script_name"."$username".home ]; then
        echo "Error: Failed to disable login! Enable it first! "
        man
        exit 1
    fi
    # Check if the user exists
    if id "$username" &>/dev/null; then
        rm -rf /tmp/"$script_name"."$username".shell /tmp/"$script_name"."$username".home
        # Store the current login shell and home directory
        getent passwd "$username" | cut -d: -f7 >/tmp/"$script_name"."$username".shell
        getent passwd "$username" | cut -d: -f6 >/tmp/"$script_name"."$username".home

        # Change the user's login shell to /sbin/nologin
        usermod -s /sbin/nologin "$username"
        echo "Login for user $username has been temporarily disabled."
    else
        echo "User $username does not exist."
    fi
    disable_login_task=0
}

# Function to enable the user's login
enable_login() {
    if [ -z "$username" ]; then
        echo "Error: Username is missing."
        man
        exit 1
    elif [ ! -f /tmp/"$script_name"."$username".shell ] ||
    [ ! -f /tmp/"$script_name"."$username".home ]; then
        echo "Error: Failed to enable login! Disable it first! "
        man
        exit 1
    fi

    # Check if the user exists
    if id "$username" &>/dev/null; then
        # Revert the login shell and home directory to the original values
        user_shell=$(cat /tmp/"$script_name"."$username".shell)
        user_home=$(cat /tmp/"$script_name"."$username".home)

        usermod -s "$user_shell" "$username"
        usermod -d "$user_home" "$username"
        rm -rf /tmp/"$script_name"."$username".shell \
        /tmp/"$script_name"."$username".home
        echo "Login for user $username has been re-enabled."
    else
        echo "User $username does not exist."
    fi
    enable_login_task=0
}

# Function to block logins from certain IPs
block_ips() {
    # Check if mailx utility is installed
    check_mailx
    if [ -z "$ip_file" ]; then
        echo "Error: IP file is missing."
        man
        exit 1
    else
        local ips=$(cat "$ip_file")
    fi

    # Check if the IP file exists
    if [ -f "$ip_file" ]; then
        # Read the IPs from the file and block incoming connections from them
        while read -r ip; do
            # First, delete any existing ACCEPT rules for the IP (if any)
            iptables -D INPUT -s "$ip" -j ACCEPT &>/dev/null
            if ! iptables -C INPUT -s "$ip" -j DROP >/dev/null 2>&1; then
                # Then, add a DROP rule for the IP
                iptables -A INPUT -s "$ip" -j DROP
                echo "Login blocked from IP: $ip"
            else
                echo "Login already blocked from IP: $ip"
                continue
            fi
        done < "$ip_file"

        # Send email notification to all local users
        local_users=$(awk -F: '($3 >= 1000 && $3 < 65534) {print $1}' /etc/passwd)
        for user in $local_users; do
            echo -e "Subject: Important Notice\n\nDear $user,\n\nLogin from the following IP addresses has been blocked on the system:\n$ips\n\nPlease contact your system administrator for more information.\n\nBest regards,\nSystem Administrator" | mail -s "Important Notice" "$user"
            echo "Email sent to user: $user"
        done
    else
        echo "IP file not found: $ip_file"
        exit 1
    fi
    block_ips_task=0
}

# Function to unblock logins from certain IPs
unblock_ips() {
    # Check if mailx utility is installed
    check_mailx
    if [ -z "$ip_file" ]; then
        echo "Error: IP file is missing."
        man
        exit 1
    else
        local ips=$(cat "$ip_file")
    fi

    # Check if the IP file exists
    if [ -f "$ip_file" ]; then
        # Read the IPs from the file and unblock incoming connections from them
        while read -r ip; do
            # First, delete the DROP rule for the IP (if it exists)
            iptables -D INPUT -s "$ip" -j DROP &>/dev/null
            if ! iptables -C INPUT -s "$ip" -j ACCEPT >/dev/null 2>&1; then
                # Then, add an ACCEPT rule for the IP
                iptables -A INPUT -s "$ip" -j ACCEPT
                echo "Login unblocked from IP: $ip"
            else
               echo "Login from IP: $ip already unblocked"
               continue
            fi
        done < "$ip_file"

        # Send email notification to all local users
        local_users=$(awk -F: '($3 >= 1000 && $3 < 65534) {print $1}' /etc/passwd)
        for user in $local_users; do
            echo -e "Subject: Important Notice\n\nDear $user,\n\nLogin from the following IP addresses has been unblocked on the system:\n$ips\n\nYou can now access the system normally.\n\nBest regards,\nSystem Administrator" | mail -s "Important Notice" "$user"
            echo "Email sent to user: $user"
        done
    else
        echo "IP file not found: $ip_file"
        exit 1
    fi
    unblock_ips_task=0
}

# Function to create a user who can execute 'iptables' without 'sudo'
create_iptables_user() {
    if [ -z "$username" ]; then
        echo "Error: Username is missing."
        man
        exit 1
    fi

    # Create the user if it doesn't exist
    if ! id "$username" &>/dev/null; then
        useradd -m "$username"
        echo "$username ALL=(ALL) NOPASSWD: /sbin/iptables" >> /etc/sudoers.d/iptables_"$username"
        # Add alias for iptables command
        echo "alias iptables='sudo /sbin/iptables'" >> "/home/$username/.bashrc"

        echo "User $username can now execute 'iptables' without 'sudo'."
    else
        echo "User $username already exists."
    fi
    iptables_user=0
}

# Function to grant read access to syslog/messages without using SUDO
grant_read_access_to_log() {
    if [ -z "$username" ]; then
        echo "Error: Username is missing."
        man
        exit 1
    fi

    # Grant read access to the log file
    if [ -f /var/log/syslog ]; then
        setfacl -m u:$username:r /var/log/syslog
        echo "User $username now has read access to /var/log/syslog."
    elif [ -f /var/log/messages ]; then
        setfacl -m u:$username:r /var/log/messages
        echo "User $username now has read access to /var/log/messages."
    else
        echo "Could not find /var/log/syslog or /var/log/messages file. Granting read access failed."
    fi
    read_access_to_log=0
}

# Function to send email
send_email() {
    # Check if mailx utility is installed
    check_mailx
    local email_output=$1

    if [ -z "$username" ]; then
        echo "Error: Username is missing."
        man
        exit 1
    fi

    # Assuming you have configured a mail server and 'mail' command is available
    echo -e "$email_output" | mail -s "Email notification" "$username@$HOSTNAME"
    echo "Email notification sent to $username@$HOSTNAME."
}

# Function to handle the script options and arguments
handle_options() {
    local has_option=0

    # Variables to capture option outputs
    local iptables_output=""
    local read_log_output=""
    local disable_login_output=""
    local enable_login_output=""
    local no_user=""

    # Parse arguments
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            -h|--help)
                man
                exit 0
                ;;
            -u|--user)
                username=$2
                shift 2
                ;;
            --password-length=*)
                password_length="${1#*=}"
                shift
                has_option=1
                length=1
                ;;
            --change-months=*)
                change_months="${1#*=}"
                shift
                has_option=1
                months=1
                ;;
            --last-passwords-count=*)
                last_passwords_count="${1#*=}"
                shift
                has_option=1
                last=1
                ;;
            --uppercase-count=*)
                uppercase_count="${1#*=}"
                shift
                has_option=1
                upper=1
                ;;
            --lowercase-count=*)
                lowercase_count="${1#*=}"
                shift
                has_option=1
                lower=1
                ;;
            --number-count=*)
                number_count="${1#*=}"
                shift
                has_option=1
                number=1
                ;;
            --special-count=*)
                special_count="${1#*=}"
                shift
                has_option=1
                special=1
                ;;
            --password-lock=*)
                password_lock="${1#*=}"
                shift
                has_option=1
                lock=1
                ;;
            --last-password-change-date=*)
                last_password_change_date="${1#*=}"
                shift
                has_option=1
                date=1
                ;;
            --deny-sudo=*)
                deny_sudo="${1#*=}"
                shift
                has_option=1
                deny=1
                ;;
            --iptables-user)
                iptables_user=1
                shift
                ;;
            --read-access-to-log)
                read_access_to_log=1
                shift
                ;;
            --send-email)
                send_email_task=1
                shift
                ;;
            --disable-login)
                disable_login_task=1
                shift
                ;;
            --enable-login)
                enable_login_task=1
                shift
                ;;
            --blacklist=*)
                ip_file="${1#*=}"
                shift
                has_option=1
                block_ips_task=1
                no_user=1
                ;;
            --whitelist=*)
                ip_file="${1#*=}"
                shift
                has_option=1
                unblock_ips_task=1
                no_user=1
                ;;
            *)
                echo "Unknown option: $1"
                man
                exit 1
                ;;
        esac
    done

    # Check if mandatory username argument is provided
    if [ -z "$username" ] && [ -z "$no_user" ]; then
        echo "Error: Username is missing."
        man
        exit 1
    fi

    # Check for task 1
    if [ -n "$username" ] && [[ "$iptables_user" -eq 1 ]]; then
        iptables_output=$(create_iptables_user)

        # Send email if --send-email option is used
        if [[ "$send_email_task" -eq 1 ]]; then
            send_email "$iptables_output"
        fi

        echo "$iptables_output"
    fi

    # Check for task 2
    if [ -n "$username" ] && [[ "$read_access_to_log" -eq 1 ]]; then
        read_log_output=$(grant_read_access_to_log)

        # Send email if --send-email option is used
        if [[ "$send_email_task" -eq 1 ]]; then
            send_email "$read_log_output"
        fi

        echo "$read_log_output"
    fi

    # Check for task 3
    if [ -n "$username" ] && [[ "$send_email_task" -eq 1 ]]; then
        send_email_output=$(send_email)
    fi

    # Check for task 4
    if [ -n "$username" ] && [[ "$disable_login_task" -eq 1 ]]; then
        disable_login_output=$(disable_login)

        # Send email if --send-email option is used
        if [[ "$send_email_task" -eq 1 ]]; then
            send_email "$disable_login_output"
        fi

        # Delay execution of other tasks for temporary login disable
        sleep 1
        echo "$disable_login_output"
    fi

    # Check for task 5
    if [ -n "$ip_file" ] && [[ "$block_ips_task" -eq 1 ]]; then
        block_ips
    fi

    # Check for task 6
    if [ -n "$ip_file" ] && [[ "$unblock_ips_task" -eq 1 ]]; then
        unblock_ips
    fi

    # Check for task 7
    if [ -n "$username" ] && [[ "$enable_login_task" -eq 1 ]]; then
        enable_login_output=$(enable_login)

        # Send email if --send-email option is used
        if [[ "$send_email_task" -eq 1 ]]; then
            send_email "$enable_login_output"
        fi

        echo "$enable_login_output"
    fi

    # Provision password should be handled last
    if [ -n "$username" ] && [[ "$has_option" -eq 1 ]]; then
        # Call the function for password provisioning
        local password_output=$(provision_password "$password_length" "$change_months" "$last_passwords_count" \
        "$uppercase_count" "$lowercase_count" "$number_count" "$special_count" "$password_lock" \
        "$last_password_change_date" "$deny_sudo")
        echo "$password_output"

        # Combine outputs for email
        local email_output="\n\n$password_output"

        # Send email if --send-email option is used
        if [[ "$send_email_task" -eq 1 ]]; then
            send_email "$email_output"
        fi
    fi
}

# Entry point of the script
script_name=$(basename -- "$0")
handle_options "$@"
