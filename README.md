<pre>
    =========================== Task 2 User Access Tool ===========================

Usage: ./task02.sh [OPTIONS]

Options:
   -h, --help                          Displays this help
   -u, --user                          Specifies the username for password provisioning
   --password-length=[length]          Sets the length of the user's password
   --change-months=[months]            Sets the password expiration period in months
   --last-passwords-count=[count]      Specifies the number of last passwords to be remembered
   --uppercase-count=[count]           Sets the minimum count of uppercase letters in the password
   --lowercase-count=[count]           Sets the minimum count of lowercase letters in the password
   --number-count=[count]              Sets the minimum count of numbers in the password
   --special-count=[count]             Sets the minimum count of special characters in the password
   --password-lock=[1 or 0]            Locks or unlocks the user's account (1 for lock, 0 for unlock)
   --last-password-change-date=[count] Sets the last password change date
   --deny-sudo=[1 or 0]                Deny / allow 'sudo su -' & 'sudo -s' for specific
                                       user (1 - deny, 0 - allow)
   --iptables-user                     Create a user who can execute 'iptables' without 'sudo'
   --read-access-to-log                Grant read access to syslog/messages without using SUDO
   --send-email                        Send a test email to the user
   --disable-login                     Disable the user's login temporarily
   --enable-login                      Enable the user's login
   --blacklist=[file]                  Block logins from certain IPs (specify a file with IPs)
   --whitelist=[file]                  Unblock logins from certain IPs (specify a file with IPs)

   Examples:
   sudo ./task02.sh -u myuser --iptables-user --read-access-to-log \
   --password-length=10 --change-months=3 --last-passwords-count=5 \
   --uppercase-count=2 --lowercase-count=2 --number-count=2 --special-count=2 \
   --password-lock=1 --last-password-change-date=0 --deny-sudo=1 --disable-login \
   --send-email --blacklist=ip.txt;
   sudo ./task02.sh -u myuser --password-lock=0 --enable-login --deny-sudo=0 \
   --send-email --whitelist=ip.txt;
   ./task02.sh -h;

   Example use cases:
   ------------------

   Task 2A

   existing user’s password length 8+ characters;
   require password changing every 3 months;
   it is not allowed to repeat 3 last passwords;
   number up case, low case, number digit and special chars;
   ask password changing when the 1st user login;
   deny/allow executing ‘sudo su -’ and ‘sudo -s’;
   prevent accidental removal of /var/log/auth.log (Debian) or /var/log/secure (RedHat).

   [ec2-user@amazonlinux ~]$ awk -F: '($3 >= 1000 && $3 < 65534) {print $1}' /etc/passwd
   ec2-user
   anna
   colin

   [colin@amazonlinux ~]$ id colin
   uid=1002(colin) gid=1002(colin) groups=1002(colin),10(wheel)

   [ec2-user@amazonlinux ~]$ sudo ./task02.sh -u colin --password-length=10 \
   --change-months=3 --last-passwords-count=3 --uppercase-count=2 \
   --lowercase-count=2 --number-count=2 --special-count=2 \
   --last-password-change-date=0 --deny-sudo=1 --send-email
   Password length for user colin is set to: 10
   Expiration period for user colin is set to: 3 month(s)
   Last passwords to be remembered for user colin is set to: 3
   Minimum count of uppercase letters for user colin is set to: 2
   Minimum count of lowercase letters for user colin is set to: 2
   Minimum count of numbers for user colin is set to: 2
   Minimum count of special characters for user colin is set to: 2
   Last password change date for user colin is set to: 0
   'sudo su -' & 'sudo -s' for user colin have been denied.
   Email notification sent to colin@amazonlinux.

   [ec2-user@amazonlinux ~]$ sudo lsattr /var/log/secure
   -----a---------- /var/log/secure

   [ec2-user@amazonlinux ~]$ sudo cat /var/spool/mail/colin
   From root@amazonlinux.localdomain  Sun Aug  6 17:15:42 2023
   Return-Path: 
   X-Original-To: colin@amazonlinux
   Delivered-To: colin@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id DEBB530356EE; Sun,  6 Aug 2023 17:15:42 +0000 (UTC)
   Date: Sun, 06 Aug 2023 17:15:42 +0000
   To: colin@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230806171542.DEBB530356EE@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   
   
   Password length for user colin is set to: 10
   Expiration period for user colin is set to: 3 month(s)
   Last passwords to be remembered for user colin is set to: 3
   Minimum count of uppercase letters for user colin is set to: 2
   Minimum count of lowercase letters for user colin is set to: 2
   Minimum count of numbers for user colin is set to: 2
   Minimum count of special characters for user colin is set to: 2
   Last password change date for user colin is set to: 0
   'sudo su -' & 'sudo -s' for user colin have been denied.

   Email notification sent to colin@amazonlinux.

   [ec2-user@amazonlinux ~]$ cat /etc/pam.d/common-password
   password required pam_pwquality.so retry=3   
   password required pam_pwquality.so minlen=10
   password required pam_pwquality.so minclass=4 minupper=2 minlower=2 minnum=2 minspecial=2
   password required pam_unix.so remember=3

   [ec2-user@amazonlinux ~]$ sudo ls -l /etc/sudoers.d/
   total 8
   -r--r----- 1 root root 135 Mar 17 18:53 90-cloud-init-users
   -rw-r--r-- 1 root root  84 Aug  6 17:15 deny_colin_su   

   [ec2-user@amazonlinux ~]$ sudo cat /etc/sudoers.d/deny_colin_su
   colin ALL = ALL, !/bin/su, !/bin/bash
   
   [colin@amazonlinux ~]$ sudo su -
   Sorry, user colin is not allowed to execute '/bin/su -' as root on amazonlinux.
   [colin@amazonlinux ~]$ sudo -s
   [sudo] password for colin:
   Sorry, user colin is not allowed to execute '/bin/bash' as root on amazonlinux.
   
   [ec2-user@amazonlinux ~]$ sudo ./task02.sh -u colin --deny-sudo=0 --send-email
   'sudo su -' & 'sudo -s' for user colin have been allowed.
   Email notification sent to colin@amazonlinux.
   [ec2-user@amazonlinux ~]$ sudo cat /var/spool/mail/colin
   
   From root@amazonlinux.localdomain  Mon Aug  7 10:41:25 2023
   Return-Path: 
   X-Original-To: colin@amazonlinux
   Delivered-To: colin@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id DF4F230356EE; Mon,  7 Aug 2023 10:41:25 +0000 (UTC)
   Date: Mon, 07 Aug 2023 10:41:25 +0000
   To: colin@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807104125.DF4F230356EE@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   
   
   'sudo su -' & 'sudo -s' for user colin have been allowed.
   
   [colin@amazonlinux ~]$ sudo su -
   Last login: Mon Aug  7 10:45:23 UTC 2023 on pts/1
   [root@amazonlinux ~]#
   sudo -s
   [root@amazonlinux colin]#

   Task 2B

   1. Create a user who can execute ‘iptables’ with any command line arguments.
   Let the user do not type ‘sudo iptables’ every time.
   2. Grant access to the user to read file /var/log/syslog (Debian) or
   /var/log/messages (RedHat) without using SUDO for the permission.
   3. Write a script to automate applying policies from the 1 and 2.
   4. Send email.
   * Temporarily disable/enable the user's login;
   ** Block/unblock logins from certain IPs. Send an email

   [ec2-user@amazonlinux ~]$ sudo ./task02.sh -u anatoli --iptables-user --read-access-to-log --send-email
   Email notification sent to anatoli@amazonlinux.
   User anatoli can now execute 'iptables' without 'sudo'.
   Email notification sent to anatoli@amazonlinux.
   User anatoli now has read access to /var/log/messages.
   
   [ec2-user@amazonlinux ~]$ awk -F: '($3 >= 1000 && $3 < 65534) {print $1}' /etc/passwd
   ec2-user
   anna
   colin
   anatoli
   [ec2-user@amazonlinux ~]$ id anatoli
   uid=1003(anatoli) gid=1003(anatoli) groups=1003(anatoli)
   
   [ec2-user@amazonlinux ~]$ sudo cat /var/spool/mail/anatoli
   
   From root@amazonlinux.localdomain  Mon Aug  7 12:55:10 2023
   Return-Path: 
   X-Original-To: anatoli@amazonlinux
   Delivered-To: anatoli@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id 9118F310E007; Mon,  7 Aug 2023 12:55:09 +0000 (UTC)
   Date: Mon, 07 Aug 2023 12:55:08 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807125510.9118F310E007@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   User anatoli now has read access to /var/log/messages.
   
   From root@amazonlinux.localdomain  Mon Aug  7 12:55:10 2023
   Return-Path: 
   X-Original-To: anatoli@amazonlinux
   Delivered-To: anatoli@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id 92E18310E008; Mon,  7 Aug 2023 12:55:09 +0000 (UTC)
   Date: Mon, 07 Aug 2023 12:55:08 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807125510.92E18310E008@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   User anatoli can now execute 'iptables' without 'sudo'.
   
   [ec2-user@amazonlinux ~]$ ls -l /var/log/messages
   -rw-r-----+ 1 root root 17323973 Aug  7 13:02 /var/log/messages
   [ec2-user@amazonlinux ~]$ getfacl  /var/log/messages
   getfacl: Removing leading '/' from absolute path names
   # file: var/log/messages
   # owner: root
   # group: root
   user::rw-
   user:colin:r--
   user:anatoli:r--
   group::---
   mask::r--
   other::---
   
   [ec2-user@amazonlinux ~]$ sudo ls -l /etc/sudoers.d/
   total 12
   -r--r----- 1 root root  135 Mar 17 18:53 90-cloud-init-users
   -rw-r--r-- 1 root root  84 Aug  6 17:15 deny_colin_su 
   -rw-r--r-- 1 root root   43 Aug  7 12:55 iptables_anatoli
   [ec2-user@amazonlinux ~]$ sudo cat /etc/sudoers.d/iptables_anatoli
   anatoli ALL=(ALL) NOPASSWD: /sbin/iptables
   
   [anatoli@amazonlinux ~]$ iptables -L
   Chain INPUT (policy ACCEPT)
   target     prot opt source               destination
   
   Chain FORWARD (policy ACCEPT)
   target     prot opt source               destination
   
   Chain OUTPUT (policy ACCEPT)
   target     prot opt source               destination
   [anatoli@amazonlinux ~]$ tail -n 10 /var/log/messages
   Aug  7 13:21:30 amazonlinux systemd: Stopping System Logging Service...
   Aug  7 13:21:30 amazonlinux rsyslogd: [origin software="rsyslogd" swVersion="8.24.0-57.amzn2.2.0.2" x-pid="2411" x-info="http://www.rsyslog.com"] exiting on signal 15.
   Aug  7 13:21:30 amazonlinux systemd: Stopped System Logging Service.
   Aug  7 13:21:30 amazonlinux systemd: Starting System Logging Service...
   Aug  7 13:21:30 amazonlinux rsyslogd: [origin software="rsyslogd" swVersion="8.24.0-57.amzn2.2.0.2" x-pid="3232" x-info="http://www.rsyslog.com"] start
   Aug  7 13:21:30 amazonlinux systemd: Started System Logging Service.
   
   sudo ./task02.sh -u anatoli --password-lock=1 --disable-login --send-ema
   il
   Email notification sent to anatoli@amazonlinux.
   Login for user anatoli has been temporarily disabled.
   Locking password for user anatoli.
   passwd: Success
   Email notification sent to anatoli@amazonlinux.
   
   [ec2-user@amazonlinux ~]$ sudo cat /var/spool/mail/anatoli
   From root@amazonlinux.localdomain  Mon Aug  7 13:28:57 2023
   Return-Path: 
   X-Original-To: anatoli@amazonlinux
   Delivered-To: anatoli@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id 429E43035EC0; Mon,  7 Aug 2023 13:28:57 +0000 (UTC)
   Date: Mon, 07 Aug 2023 13:28:57 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807132857.429E43035EC0@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   Login for user anatoli has been temporarily disabled.
   
   From root@amazonlinux.localdomain  Mon Aug  7 13:28:58 2023
   Return-Path: 
   X-Original-To: anatoli@amazonlinux
   Delivered-To: anatoli@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id 8656D3035EC0; Mon,  7 Aug 2023 13:28:58 +0000 (UTC)
   Date: Mon, 07 Aug 2023 13:28:58 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807132858.8656D3035EC0@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   
   
   Locking password for user anatoli.
   passwd: Success
   
   [ec2-user@amazonlinux ~]$ sudo su - anatoli
   Last login: Mon Aug  7 13:24:12 UTC 2023 on pts/1
   This account is currently not available.
   
   [ec2-user@amazonlinux ~]$ sudo ./task02.sh -u anatoli --password-lock=0 --enable-login --send-emai
   l
   usermod: no changes
   Email notification sent to anatoli@amazonlinux.
   Login for user anatoli has been re-enabled.
   Unlocking password for user anatoli.
   passwd: Success
   Email notification sent to anatoli@amazonlinux.
   
   [ec2-user@amazonlinux ~]$ sudo su - anatoli
   Last login: Mon Aug  7 13:30:11 UTC 2023 on pts/1
   Last failed login: Mon Aug  7 13:38:07 UTC 2023 on tty1
   There were 4 failed login attempts since the last successful login.
   
   [anatoli@amazonlinux ~]$ mail
   Heirloom Mail version 12.5 7/5/10.  Type ? for help.
   "/var/spool/mail/anatoli": 9 messages 7 unread
   >U  1 root                  Mon Aug  7 12:55  19/658   "Email notification"
    U  2 root                  Mon Aug  7 12:55  19/712   "Email notification"
       3 root                  Mon Aug  7 12:55  19/714   "Email notification"
       4 root                  Mon Aug  7 13:28  19/659   "Email notification"
    U  5 root                  Mon Aug  7 13:28  19/711   "Email notification"
    U  6 root                  Mon Aug  7 13:28  22/710   "Email notification"
    U  7 root                  Mon Aug  7 13:37  19/658   "Email notification"
    U  8 root                  Mon Aug  7 13:37  19/701   "Email notification"
    U  9 root                  Mon Aug  7 13:37  22/712   "Email notification"
   & 8
   Message  8:
   From root@amazonlinux.localdomain  Mon Aug  7 13:37:52 2023
   Return-Path: 
   X-Original-To: anatoli@amazonlinux
   Delivered-To: anatoli@amazonlinux.localdomain
   Date: Mon, 07 Aug 2023 13:37:52 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   Content-Type: text/plain; charset=us-ascii
   From: root@amazonlinux.localdomain (root)
   Status: RO
   
   Login for user anatoli has been re-enabled.
   
   &
   Message  9:
   From root@amazonlinux.localdomain  Mon Aug  7 13:37:52 2023
   Return-Path: 
   X-Original-To: anatoli@amazonlinux
   Delivered-To: anatoli@amazonlinux.localdomain
   Date: Mon, 07 Aug 2023 13:37:52 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Email notification
   User-Agent: Heirloom mailx 12.5 7/5/10
   Content-Type: text/plain; charset=us-ascii
   From: root@amazonlinux.localdomain (root)
   Status: RO
   
   
   
   Unlocking password for user anatoli.
   passwd: Success
   
   [ec2-user@amazonlinux ~]$ cat ip.txt
   10.0.2.13
   10.0.2.14
   10.0.2.15
   
   [ec2-user@amazonlinux ~]$ sudo ./task02.sh --blacklist=ip.txt
   Login blocked from IP: 10.0.2.13
   Login blocked from IP: 10.0.2.14
   Login blocked from IP: 10.0.2.15
   Email sent to user: ec2-user
   Email sent to user: anna
   Email sent to user: colin
   Email sent to user: anatoli
   
   [anatoli@amazonlinux ~]$
   You have new mail in /var/spool/mail/anatoli
   [anatoli@amazonlinux ~]$ mail
   [anatoli@amazonlinux ~]$ mail
   Heirloom Mail version 12.5 7/5/10.  Type ? for help.
   "/var/spool/mail/anatoli": 12 messages 1 unread
       1 root                  Mon Aug  7 12:55  19/659   "Email notification"
       2 root                  Mon Aug  7 12:55  19/713   "Email notification"
       3 root                  Mon Aug  7 12:55  19/714   "Email notification"
       4 root                  Mon Aug  7 13:28  19/659   "Email notification"
       5 root                  Mon Aug  7 13:28  19/712   "Email notification"
       6 root                  Mon Aug  7 13:28  22/711   "Email notification"
       7 root                  Mon Aug  7 13:37  19/659   "Email notification"
       8 root                  Mon Aug  7 13:37  19/702   "Email notification"
       9 root                  Mon Aug  7 13:37  22/713   "Email notification"
   >U 10 root                  Mon Aug  7 14:18  31/887   "Important Notice"
      11 root                  Mon Aug  7 14:23  31/888   "Important Notice"
      12 root                  Mon Aug  7 14:26  31/886   "Important Notice"
   & 12
   Message 12:
   From root@amazonlinux.localdomain  Mon Aug  7 14:26:50 2023
   Return-Path: 
   X-Original-To: anatoli
   Delivered-To: anatoli@amazonlinux.localdomain
   Date: Mon, 07 Aug 2023 14:26:50 +0000
   To: anatoli@amazonlinux.localdomain
   Subject: Important Notice
   User-Agent: Heirloom mailx 12.5 7/5/10
   Content-Type: text/plain; charset=us-ascii
   From: root@amazonlinux.localdomain (root)
   Status: RO
   
   Subject: Important Notice
   
   Dear anatoli,
   
   Login from the following IP addresses has been blocked on the system:
   10.0.2.13
   10.0.2.14
   10.0.2.15
   
   Please contact your system administrator for more information.
   
   Best regards,
   System Administrator
   
   & Held 12 messages in /var/spool/mail/anatoli
   
   [ec2-user@amazonlinux ~]$
   You have new mail in /var/spool/mail/ec2-user
   [ec2-user@amazonlinux ~]$ mail
   Heirloom Mail version 12.5 7/5/10.  Type ? for help.
   "/var/spool/mail/ec2-user": 4 messages 4 new
   >N  1 (Cron Daemon)         Mon Aug  7 14:00  25/1029  "Cron  sud"
    N  2 root                  Mon Aug  7 14:18  30/881   "Important Notice"
    N  3 root                  Mon Aug  7 14:23  30/881   "Important Notice"
    N  4 root                  Mon Aug  7 14:26  30/879   "Important Notice"
   & 4
   Message  4:
   From root@amazonlinux.localdomain  Mon Aug  7 14:26:50 2023
   Return-Path: 
   X-Original-To: ec2-user
   Delivered-To: ec2-user@amazonlinux.localdomain
   Date: Mon, 07 Aug 2023 14:26:50 +0000
   To: ec2-user@amazonlinux.localdomain
   Subject: Important Notice
   User-Agent: Heirloom mailx 12.5 7/5/10
   Content-Type: text/plain; charset=us-ascii
   From: root@amazonlinux.localdomain (root)
   Status: R
   
   Subject: Important Notice
   
   Dear ec2-user,
   
   Login from the following IP addresses has been blocked on the system:
   10.0.2.13
   10.0.2.14
   10.0.2.15
   
   Please contact your system administrator for more information.
   
   Best regards,
   System Administrator
   
   & Held 4 messages in /var/spool/mail/ec2-user
   
   [ec2-user@amazonlinux ~]$ hostname -I
   10.0.2.8 192.168.0.151
   ubuntu@ip-172-31-42-130:~$ hostname -I
   10.0.2.13 192.168.0.77
   
   ubuntu@ip-172-31-42-130:~$ ssh -i .ssh/"j2.pem" ec2-user@10.0.2.8
   ssh: connect to host 10.0.2.8 port 22: Connection timed out
   
   [ec2-user@amazonlinux ~]$ sudo ./task02.sh --whitelist=ip.txt
   Login unblocked from IP: 10.0.2.13
   Login unblocked from IP: 10.0.2.14
   Login unblocked from IP: 10.0.2.15
   Email sent to user: ec2-user
   Email sent to user: anna
   Email sent to user: colin
   Email sent to user: anatoli
   
   [ec2-user@amazonlinux ~]$ sudo cat /var/spool/mail/colin
   From root@amazonlinux.localdomain  Mon Aug  7 14:43:28 2023
   Return-Path: 
   X-Original-To: colin
   Delivered-To: colin@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id 6A38E3007851; Mon,  7 Aug 2023 14:43:28 +0000 (UTC)
   Date: Mon, 07 Aug 2023 14:43:28 +0000
   To: colin@amazonlinux.localdomain
   Subject: Important Notice
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807144328.6A38E3007851@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   Subject: Important Notice
   
   Dear colin,
   
   Login from the following IP addresses has been unblocked on the system:
   10.0.2.13
   10.0.2.14
   10.0.2.15
   
   You can now access the system normally.
   
   Best regards,
   System Administrator
   
   [ec2-user@amazonlinux ~]$ sudo cat /var/spool/mail/anna
   From root@amazonlinux.localdomain  Mon Aug  7 14:43:28 2023
   Return-Path: 
   X-Original-To: anna
   Delivered-To: anna@amazonlinux.localdomain
   Received: by amazonlinux.localdomain (Postfix, from userid 0)
           id 61E723007841; Mon,  7 Aug 2023 14:43:28 +0000 (UTC)
   Date: Mon, 07 Aug 2023 14:43:28 +0000
   To: anna@amazonlinux.localdomain
   Subject: Important Notice
   User-Agent: Heirloom mailx 12.5 7/5/10
   MIME-Version: 1.0
   Content-Type: text/plain; charset=us-ascii
   Content-Transfer-Encoding: 7bit
   Message-Id: <20230807144328.61E723007841@amazonlinux.localdomain>
   From: root@amazonlinux.localdomain (root)
   
   Subject: Important Notice
   
   Dear anna,
   
   Login from the following IP addresses has been unblocked on the system:
   10.0.2.13
   10.0.2.14
   10.0.2.15
   
   You can now access the system normally.
   
   Best regards,
   System Administrator
   
   ubuntu@ip-172-31-42-130:~$ ssh -i .ssh/"j2.pem" ec2-user@10.0.2.8
   Last login: Mon Aug  7 13:07:17 2023 from 192.168.0.80
   
          __|  __|_  )
          _|  (     /   Amazon Linux 2 AMI
         ___|\___|___|
   
   https://aws.amazon.com/amazon-linux-2/
   [ec2-user@amazonlinux ~]$ logout
   Connection to 10.0.2.8 closed.

</pre>
